#include "CPen.h"

CDX9Pen cDX9Pen;
CPen* cPen;

//===================================================================================

void CPen::Fill( float x, float y, float w, float h, DWORD dwColor )
{
	FillRGBA( x, y, w, h, dwColor );
}

//===================================================================================

void CPen::FillCenter( float x, float y, float w, float h, DWORD dwColor )
{
	FillRGB( x - ( w / 2 ), y - ( h / 2 ), w, h, dwColor );
}

//===================================================================================

void CPen::Textf(float x, float y, DWORD dwColor, const char *szText, ...) 
{
	static char szBuffer[ 4096 ];

	va_list va_alist;
	va_start( va_alist, szText );
	vsnprintf( szBuffer, sizeof( szBuffer ), szText, va_alist );
	va_end( va_alist );

	Text( x, y, dwColor, szBuffer );
}

//===================================================================================

void CPen::TextCenteredf(float x, float y, DWORD dwColor, const char *szText, ...) 
{
	static char szBuffer[ 4096 ];

	va_list va_alist;
	va_start( va_alist, szText );
	vsnprintf( szBuffer, sizeof( szBuffer ), szText, va_alist );
	va_end( va_alist );

	Text( x - TextWidth( szBuffer ) / 2, y, dwColor, szBuffer );
}

//===================================================================================

void CPen::Circle( float x, float y, float r, float s, DWORD dwColor ) 
{
	float Step = PI * 2.0 / s;
	for( float a = 0; a < ( PI*2.0 ); a += Step ) 
	{
		float x1 = r * cos( a )        + x;
		float y1 = r * sin( a )        + y;
		float x2 = r * cos( a + Step ) + x;
		float y2 = r * sin( a + Step ) + y;

		Line( x1, y1, x2, y2, dwColor );
	}
}

//===================================================================================

void CPen::Box( float x, float y, float w, float h, DWORD dwColor )
{
	Line(     x,     y, x + w,     y, dwColor );
	Line( x + w,     y, x + w, y + h, dwColor );
	Line( x + w, y + h,     x, y + h, dwColor );
	Line(     x, y + h,     x,     y, dwColor );
}

//===================================================================================

void CPen::BoxCentered( float x, float y, float w, float h, DWORD dwColor )
{
	Box( x - w / 2, y - h / 2, w, h, dwColor );
}




