#pragma once

#ifndef __CLOG_HEADER__
#define __CLOG_HEADER__

#define _CRT_SECURE_NO_WARNINGS

#include <windows.h>
#include <string>
#include <time.h>
#include "..\Includes.h"

#define LOG_NONE	0
#define LOG_CONSOLE 1
#define LOG_FILE	2
#define LOG_BOTH	3

using namespace std;

class CLog
{
public:

	void LogBaseUponModule( HMODULE hmModule );

	char* GetDirectoryFile( char* szFile );

	void BuildDebugConsole( const char* szTitle );

	void Log( const char* pszMessage, ... );

private:
	char	szDirectory[ 255 ];
	char	szFile[ 32 ];
	DWORD	dwMessage;
	HANDLE	hDebugConsole;
};

extern CLog cLog;

#endif