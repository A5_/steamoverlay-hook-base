#include "CSig.h"

CSig cSig;


CSig::CSig()
{
	Clear();
}

void CSig::Clear()
{
	vSigs.clear();
}

bool CSig::SetRange( DWORD ulMin, DWORD ulMax )
{
	ulRangeMin = ulMin;
	ulRangeMax = ulMax;
	return true;
}

bool CSig::SetRange( HMODULE hModule )
{
	if (hModule == NULL || hModule == INVALID_HANDLE_VALUE)
		return false;

	PIMAGE_DOS_HEADER DosHead = (IMAGE_DOS_HEADER*)hModule;

	if (DosHead->e_magic != IMAGE_DOS_SIGNATURE)
		return false;

	PIMAGE_NT_HEADERS NTHead =
		(PIMAGE_NT_HEADERS)((DWORD)DosHead + (DWORD)DosHead->e_lfanew);

	if (NTHead->Signature != IMAGE_NT_SIGNATURE)
		return false;

	DWORD address = (DWORD)hModule + NTHead->OptionalHeader.BaseOfCode;
	DWORD size = address + NTHead->OptionalHeader.SizeOfCode;

	ulRangeMin = ( DWORD )address;
	ulRangeMax = ( DWORD )size;

	return true;
}

FSig* CSig::ManualAdd( const char* pszName, DWORD ulAddressOf )
{
	FSig Use;

	Use.ulAddress = ulAddressOf;

	strcpy( Use.szName, pszName );

	vSigs.push_back( Use );

	return GetSigByIndex( this->vSigs.size() - 1 );
}

FSig* CSig::AddSig( const char* pszName, BYTE* pbMask, CHAR* pszMask, DWORD offset )
{
	if( ulRangeMin == NULL || ulRangeMax == NULL )
		return FALSE;

	if( pszName == NULL || pbMask == NULL || pszMask == NULL)
		return FALSE;

	DWORD ulFound = DetectPattern( pbMask, pszMask );

	if( ulFound == 0 )
		return 0;

	ulFound = *( DWORD* )( ulFound + offset );

	return ManualAdd( pszName, ulFound );
}

FSig* CSig::GetSigByName( const char* pszName )
{
	for( size_t i = 0; i < vSigs.size(); ++i )
	{
		if( !_stricmp( vSigs[ i ].szName, pszName ) )
			return &vSigs[ i ];
	}
	return NULL;
}

FSig* CSig:: GetSigByIndex( int iIndex )
{
	return &vSigs[ iIndex ];
}

int CSig::GetSigCount()
{
	return GetVector().size();
}

vector< FSig > CSig::GetVector()
{
	return vSigs;
}


DWORD CSig::GetRangeMax()
{
	return ulRangeMax;
}


DWORD CSig::GetRangeMin()
{
	return ulRangeMin;
}


bool CSig::DataComp( BYTE* pbData, BYTE* pbMask, const char* pszMask )
{
	for( ; *pszMask; ++pszMask, ++pbData, ++pbMask )
	{
		if( *pszMask == 'x' && *pbData != *pbMask )
			return false;
	}
	
	return ( *pszMask == NULL );
}

unsigned long CSig::DetectPattern( BYTE *pbMask, const char *pszMask )
{
	for( DWORD i = 0; i < ulRangeMax; ++i )
	{
		if( DataComp( ( BYTE* )( ulRangeMin + i ), pbMask, pszMask ) )
			return ulRangeMin + i;
	}

	return 0;
}



