
#include "Overlay.h"

Present_T Present_O;

float x = 0;

HRESULT _stdcall Present_H( LPDIRECT3DDEVICE9 pDevice, RECT* pSourceRect, RECT* pDestRect, HWND hDestWindowOverride, RGNDATA* pDirtyRegion ) 
{
	cPen->Init( pDevice );

	++x;
							  
	if( x >= 500 )
		x = 0;

	cPen->Text( x, 10, D3DCOLOR_RGBA( 255, 0, 0, 255 ), "TEAMGAMERFOOD" );

	cPen->Fill( 50, 50, 100, 100, D3DCOLOR_RGBA( 255, 0, 0, 100 ) );

	cPen->Reset( );

	return Present_O( pDevice, pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion );
}

